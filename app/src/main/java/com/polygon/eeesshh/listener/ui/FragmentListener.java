package com.polygon.eeesshh.listener.ui;


import com.polygon.eeesshh.listener.IListener;

public interface FragmentListener extends IListener {

    void switchView(int viewId);
}
