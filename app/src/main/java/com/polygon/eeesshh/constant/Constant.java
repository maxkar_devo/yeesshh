package com.polygon.eeesshh.constant;


public class Constant {

    public static class WebConstant {

        public static final String DEEP_LINK_PREFIX = "intent";
    }

    public static class Mock {

        public static final String HTML_MARKUP  =
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<h2>PLAYWING</h2>\n" +
                "<p><a href=\"http://store.playwing.com/lp?cid=5a958a4a453a3&pk_campaign=1&pk_kwd=804&kp=testValue\">Visit Playwing Store</a></p>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        /*public static final String HTML_MARKUP  =
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<h2>PLAYWING</h2>\n" +
                "<p><a href=\"intent://store.playwing.com/lp?cid=5a958a4a453a3&pk_campaign=1&pk_kwd=804&kp=testValue%23Intent;scheme=http;package=com.android.chrome;end\">Visit Playwing Store</a></p>\n" +
                "\n" +
                "</body>\n" +
                "</html>";*/

//        public static final String TEST_DEEP_LINK = "intent://userprotection.tech/d5/spc.php#Intent;scheme=http;package=com.android.chrome;end";
//        public static final String TEST_DEEP_LINK = "http://store.playwing.com/lp?cid=58d8d511a3a0b&pk_campaign=2&pk_kwd=196&s1=454353453";
//        public static final String TEST_DEEP_LINK = "http://store.playwing.com/lp?cid=59d75cac71652&pk_campaign=31&pk_kwd=571&clickid=432432423";

        public static final String TEST_DEEP_LINK = "http://store.playwing.com/lp?cid=5a958a4a453a3&pk_campaign=1&pk_kwd=804&kp=testValue";

//        public static final String TEST_DEEP_LINK = "http://store.playwing.com/_debug_";
    }
}
