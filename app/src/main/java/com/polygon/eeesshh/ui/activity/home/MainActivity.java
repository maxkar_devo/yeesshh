package com.polygon.eeesshh.ui.activity.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.polygon.eeesshh.R;
import com.polygon.eeesshh.listener.ui.FragmentListener;
import com.polygon.eeesshh.ui.activity.BasicActivity;
import com.polygon.eeesshh.ui.fragment.home.HomeFragment;
import com.polygon.eeesshh.ui.fragment.web.SystemWebFragment;
import com.polygon.eeesshh.ui.fragment.web.WebFragment;
import com.polygon.eeesshh.ui.fragment.web.WebPageFragment;


public class MainActivity extends BasicActivity implements FragmentListener{

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int INTERNET_PERMISSION_REQUEST = 999;

    private HomeFragment homeFragment;
    private WebFragment webFragment;
    private WebPageFragment webPageFragment;
    private SystemWebFragment systemPageFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                == PackageManager.PERMISSION_GRANTED) {

            setupLayoutComponents();
            setCurrentFragment(0);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    INTERNET_PERMISSION_REQUEST);
        }

        initComponents();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @SuppressWarnings("RedundantCast")
    @Override
    protected void initComponents() {

    }

    private void setupLayoutComponents() {

        //initialize  fragments instances
        homeFragment = new HomeFragment();
        webFragment = new WebFragment();
        webPageFragment = new WebPageFragment();
        systemPageFragment = new SystemWebFragment();
    }

    /**
     * Replace old fragment with new one
     */
    public void setCurrentFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                replaceFragment(homeFragment);
                break;
            case 1:
                replaceFragment(webFragment);
                break;
            case 2:
                replaceFragment(webPageFragment);
                break;
            case 3:
                replaceFragment(systemPageFragment);
                break;
            default:
                replaceFragment(homeFragment);
                break;
        }
    }

    /**
     * Replace fragment procedure
     *
     * @param fragment new chosed procedure
     */
    public void replaceFragment(Fragment fragment) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.mainViewHolder, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case INTERNET_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setupLayoutComponents();
                    setCurrentFragment(0);

                } else {
                    Log.d(TAG, "Permission request sent");
                }
            }
        }
    }

    @Override
    public void switchView(int viewId) {
        setCurrentFragment(viewId);
    }

    @Override
    public void onBackPressed() {
        setCurrentFragment(0);
    }
}
