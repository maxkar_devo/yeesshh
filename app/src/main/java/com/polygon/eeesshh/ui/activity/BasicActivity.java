package com.polygon.eeesshh.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.polygon.eeesshh.R;


public abstract class BasicActivity extends AppCompatActivity implements BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getContentView() > 0)
            setContentView(getContentView());

        commonStyling();
    }

    public void commonStyling() {

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            getSupportActionBar().setDisplayOptions(
                    ActionBar.DISPLAY_SHOW_HOME
                            | ActionBar.DISPLAY_SHOW_TITLE
                            | ActionBar.DISPLAY_USE_LOGO
                    /*| ActionBar.DISPLAY_HOME_AS_UP*/);

            actionBar.setLogo(R.drawable.yeesshh_logo);
            //actionBar.setHomeAsUpIndicator(R.drawable.logo);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected abstract int getContentView();

    protected abstract void initComponents();

}

