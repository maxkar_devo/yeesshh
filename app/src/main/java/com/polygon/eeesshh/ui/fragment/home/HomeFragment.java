package com.polygon.eeesshh.ui.fragment.home;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

import com.polygon.eeesshh.R;
import com.polygon.eeesshh.constant.Constant;
import com.polygon.eeesshh.listener.ui.FragmentListener;
import com.polygon.eeesshh.tool.client.EmpiricalWVClient;
import com.polygon.eeesshh.ui.fragment.BasicFragment;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class HomeFragment extends BasicFragment {

    private static final String TAG = HomeFragment.class.getSimpleName();

    private FragmentListener fragmentListener;

    private Button externalViewBtn;
    private Button internalViewBtn;
    private Button htmlViewBtn;
    private Button htmlBrowserViewBtn;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentListener = (FragmentListener) getActivity();
        initComponents();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home;
    }

    @SuppressWarnings({"RedundantCast", "ConstantConditions"})
    @Override
    protected void initComponents() {

        externalViewBtn = (Button) getView().findViewById(R.id.btn_external_wc);
        internalViewBtn = (Button) getView().findViewById(R.id.btn_internal_wc);
        htmlViewBtn = (Button) getView().findViewById(R.id.btn_html_wc);
        htmlBrowserViewBtn = (Button) getView().findViewById(R.id.btn_browser_html_wc);

        externalViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                externalRendering();
            }
        });

        internalViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentListener.switchView(1);
            }
        });

        htmlViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentListener.switchView(2);
            }
        });

        htmlBrowserViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentListener.switchView(3);
            }
        });

    }

    @Override
    protected void initListener(String methodName) {
        Log.d(TAG, "initListener: ");
    }

    @Override
    protected void initLogger(String methodName) {
        Log.d(TAG, "initLogger: ");
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void renderWebPage() {
        WebView contentView = new WebView(getActivity());
        EmpiricalWVClient wvClient = new EmpiricalWVClient();
        WebSettings webSettings = contentView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        contentView.setWebViewClient(wvClient);
        contentView.loadUrl(Constant.Mock.TEST_DEEP_LINK);
    }

    private void externalRendering(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(Constant.Mock.TEST_DEEP_LINK));
        startActivity(i);
    }
}
