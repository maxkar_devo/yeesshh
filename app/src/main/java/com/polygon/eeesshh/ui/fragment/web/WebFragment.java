package com.polygon.eeesshh.ui.fragment.web;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.polygon.eeesshh.R;
import com.polygon.eeesshh.constant.Constant;
import com.polygon.eeesshh.ui.fragment.BasicFragment;

public class WebFragment extends BasicFragment{

    private static final String TAG = WebFragment.class.getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private WebView mWebview ;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        renderWebView();
    }

    @SuppressWarnings({"ConstantConditions", "RedundantCast"})
    @SuppressLint("SetJavaScriptEnabled")
    private void renderWebView() {

        final Activity activity = getActivity();

        mWebview  = (WebView)getView().findViewById(R.id.wv_main_content);

        mWebview.getSettings().setJavaScriptEnabled(true);

        mWebview.setWebViewClient(new WebViewClient() {

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        mWebview.loadUrl(Constant.Mock.TEST_DEEP_LINK);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_web;
    }

    @Override
    protected void initComponents() {
        Log.d(TAG, "initComponents: ");
    }

    @Override
    protected void initListener(String methodName) {
        Log.d(TAG, "initListener: ");
    }

    @Override
    protected void initLogger(String methodName) {
        Log.d(TAG, "initLogger: ");
    }
}
