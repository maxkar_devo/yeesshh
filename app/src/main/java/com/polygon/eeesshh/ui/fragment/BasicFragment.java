package com.polygon.eeesshh.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polygon.eeesshh.tool.common.DateTimeTool;


public abstract class BasicFragment extends Fragment implements BaseFragment {

    protected Activity activity;
    protected Resources resources;

    protected StringBuilder logsBuilder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activity = getActivity();
        resources = activity.getResources();
        logsBuilder = new StringBuilder("Polygon")
                .append("\n");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (getLayoutResId() != 0)
            return inflater.inflate(getLayoutResId(), container, false);
        else
            return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        logsBuilder.setLength(0);
        logsBuilder.trimToSize();
    }

    protected abstract int getLayoutResId();

    protected abstract void initComponents();

    protected abstract void initListener(String methodName);

    protected abstract void initLogger(String methodName);

    protected String modifyListenerMethodName(String methodName) {

        return methodName;
    }

    protected String modifyXMediateLogs(String methodName) {

        return logsBuilder
                .append("\n")
                .append(DateTimeTool.getLoggerTime())
                .append("/-------/")
                .append(methodName)
                .toString();
    }
}
