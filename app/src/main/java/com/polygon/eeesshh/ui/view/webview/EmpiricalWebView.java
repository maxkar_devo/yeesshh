package com.polygon.eeesshh.ui.view.webview;


import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class EmpiricalWebView extends WebView {

    public EmpiricalWebView(Context context) {
        super(context);
    }

    public EmpiricalWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmpiricalWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
