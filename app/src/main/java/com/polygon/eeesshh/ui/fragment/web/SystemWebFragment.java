package com.polygon.eeesshh.ui.fragment.web;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.polygon.eeesshh.R;
import com.polygon.eeesshh.constant.Constant;
import com.polygon.eeesshh.ui.fragment.BasicFragment;


public class SystemWebFragment extends BasicFragment {

    private static final String TAG = SystemWebFragment.class.getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private WebView mWebview;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        renderWebView();
    }

    @SuppressWarnings({"ConstantConditions", "RedundantCast"})
    @SuppressLint("SetJavaScriptEnabled")
    private void renderWebView() {

        mWebview = (WebView) getView().findViewById(R.id.wv_main_content_s_p);

        mWebview.getSettings().setJavaScriptEnabled(true);

        mWebview.loadUrl("file:///android_asset/mock.html");
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_system_page;
    }

    @Override
    protected void initComponents() {
        Log.d(TAG, "initComponents: ");
    }

    @Override
    protected void initListener(String methodName) {
        Log.d(TAG, "initListener: ");
    }

    @Override
    protected void initLogger(String methodName) {
        Log.d(TAG, "initLogger: ");
    }

    private void externalRendering() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(Constant.Mock.TEST_DEEP_LINK));
        startActivity(i);
    }
}
