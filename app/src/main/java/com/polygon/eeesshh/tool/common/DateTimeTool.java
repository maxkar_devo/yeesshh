package com.polygon.eeesshh.tool.common;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeTool {

    public static String getLoggerTime(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ROOT);
        Date now = new Date();
        return sdf.format(now);
    }
}
